var RouterContainer = require('../Services/RouterContainer');
var AppDispatcher = require('../dispatchers/AppDispatcher');
var loginStore = require('../stores/LoginStore');
var LoginActions = (function() {
    var loginUser = function(user, cred) {
        //RouterContainer.get().transitionTo('home');
        RouterContainer.get().transitionTo(loginStore.getTransitionPath() || '/');
        localStorage.setItem('token', 'cred');
        AppDispatcher.dispatch({
            actionType: 'LOGIN_USER',
            cred: cred,
            user: user
        });
        //console.log(loginStore.getTransitionPath());
        //RouterContainer.get().transitionTo(loginStore.getTransitionPath() || '/');
    };
    var logoutUser = function(user, cred) {
        RouterContainer.get().transitionTo('home');
        localStorage.removeItem('token');
        AppDispatcher.dispatch({
            actionType: 'LOGOUT_USER',
            cred: cred,
            user: user
        });
    };
    return ({
        loginUser: loginUser,
        logoutUser: logoutUser
    });
})();
module.exports = LoginActions;