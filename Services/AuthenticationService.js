var loginActions = require('../actions/LoginAction');
var AuthService = (function() {
    var login = function(user, cred) {
        if (cred === '1111') {
            loginActions.loginUser(user, cred);
            return true;
        }
        return false;
    };
    var logout = function() {
        loginActions.logoutUser();
    };
    return ({
        login: login,
        logout: logout
    });
})();
module.exports = AuthService;