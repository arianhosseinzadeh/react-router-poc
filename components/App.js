'use strict';

var React = require('react');
var Router = require('react-router');
var RouteHandler = Router.RouteHandler;
var Link = Router.Link;
var App = React.createClass({

    displayName: 'App',

    getDefaultProps: function() {
        return {
            message: 'The Default Prop for App'
        };
    },

    mixins: [],
    render: function() {
        return (<div>
            <h1>Welcome to react router poc : </h1>
            {this.props.message}
            <RouteHandler />
        </div>);
    }
});

module.exports = App;
