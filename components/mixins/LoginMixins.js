var loginStore = require('../../stores/LoginStore');
var AuthMixins = {
    statics: {
        willTransitionTo(transition) {
            loginStore.setTransitionPath(transition.path);
            console.log(loginStore.id);
            if (!loginStore.isLoggedIn()) {
                //transition.redirect('login', {}, {'nextPath': transition.path});
                transition.redirect('login');
            }
        }
    },
    _getLoginState() {
        return {
            userLoggedIn: loginStore.isLoggedIn()
        };
    },
    componentDidMount() {
        this.changeListener = this._onChange;
        loginStore.addChangeListener(this.changeListener);
    },
    _onChange() {
        this.setState(this._getLoginState());
    },
    getInitialState() {
        return {
            userLoggedIn: this._getLoginState()
        };
    },
    componentWillUnmount: function() {
        loginStore.removeChangeListener(this.changeListener);
    }
};
module.exports = AuthMixins;