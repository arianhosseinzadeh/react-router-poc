'use strict';

var React = require('react');
var AuthMixins = require('./mixins/LoginMixins');
var loginStore = require('../stores/LoginStore');
var About = React.createClass({
    displayName: 'About',
    statics: {
        willTransitionTo(transition) {
            if (!loginStore.isLoggedIn()) {
                loginStore.setTransitionPath(transition.path);
                //transition.redirect('login', {}, {'nextPath': transition.path});
                transition.redirect('login');
            }
        }
    },
    _getLoginState() {
        return {
            userLoggedIn: loginStore.isLoggedIn()
        };
    },
    componentDidMount() {
        this.changeListener = this._onChange;
        loginStore.addChangeListener(this.changeListener);
    },
    _onChange() {
        this.setState(this._getLoginState());
    },
    getInitialState() {
        return {
            userLoggedIn: this._getLoginState()
        };
    },
    componentWillUnmount: function() {
        loginStore.removeChangeListener(this.changeListener);
    },
    getDefaultProps: function() {
        return {
            message: 'Default Prop for About page'
        };
    },
    render: function() {
        return (<div>
        {this.props.message}
        </div>);
    }
});

module.exports = About;
