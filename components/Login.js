'use strict';

var React = require('react');
var AuthenticationServices = require('../Services/AuthenticationService');
var Login = React.createClass({

    displayName: 'Login',

    propTypes: {},

    getDefaultProps: function() {
        return {
            message: 'Default Prop for Login page'
        };
    },
    _login: function(e) {
        e.preventDefault();
        AuthenticationServices.login('arian', '1111');
    },
    _logout: function() {
        e.preventDefault();
        AuthenticationServices.logout();
    },
    render: function() {
        return (<div>
            <button type="button" onClick={this._login}>Log in</button>
            <button type="button" onClick={this._logout}>Log out</button>
        </div>);
    }
});

module.exports = Login;
