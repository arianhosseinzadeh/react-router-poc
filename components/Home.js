'use strict';

var React = require('react');
var Router = require('react-router');
var Link = Router.Link;
var loginStore = require('../stores/LoginStore');
var Home = React.createClass({

    displayName: 'Home',

    propTypes: {},

    statics: {
        willTransitionTo(transition) {
            if (!loginStore.isLoggedIn()) {
                loginStore.setTransitionPath(transition.path);
                transition.redirect('login');
            }
        }
    },
    _getLoginState() {
        return {
            userLoggedIn: loginStore.isLoggedIn()
        };
    },
    componentDidMount() {
        this.changeListener = this._onChange;
        loginStore.addChangeListener(this.changeListener);
    },
    _onChange() {
        this.setState(this._getLoginState());
    },
    getInitialState() {
        return {
            userLoggedIn: this._getLoginState()
        };
    },
    componentWillUnmount: function() {
        loginStore.removeChangeListener(this.changeListener);
    },

    getDefaultProps: function() {
        return {
            message: 'Default Prop for Home page'
        };
    },

    render: function() {
        return (<div>
            {this.props.message}
            <br/>
            <Link to='about'> About </Link>
            <br/>
            <Link to="products"> Products </Link>
        </div>);
    }
});

module.exports = Home;
