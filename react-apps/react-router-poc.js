var React = require('react');
var initialState = JSON.parse(document.getElementById('initial-state').innerHTML);
var Router = require('react-router');
var routes = require('../routes/routesTree');
var RouterContainer = require('../Services/RouterContainer');
var router = Router.create({routes: routes, location: Router.HistoryLocation});
RouterContainer.set(router);
router.run(function(Handler, state) {
    React.render(
        <Handler {...initialState} />,
        document.getElementById('react-app')
    );
});