var AppDispatcher = require('../dispatchers/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var LoginStore = (function() {
    var _cred = null,
        _user = null,
        id = Date.now(),
        transitionPath = null;
    var eventEmitter = new EventEmitter();
    var _listenToLogin = function(action) {
        if (action.actionType === 'LOGIN_USER') {
            _cred = action.cred;
            _user = action.user;
            eventEmitter.emit('CHANGE');
        } else if (action.actionType === 'LOGOUT_USER') {
            _cred = null;
            _user = null;
            eventEmitter.emit('CHANGE');
        }
    };
    AppDispatcher.register(_listenToLogin.bind(this));
    var user = function() {
        return _user;
    };
    var cred = function() {
        return _cred;
    };
    var addChangeListener = function(cb) {
        eventEmitter.on('CHANGE', cb);
    };
    var removeChangeListener = function(cb) {
        eventEmitter.removeListener('CHANGE', cb);
    };
    var isLoggedIn = function() {
        return (!!_user);
    };
    var setTransitionPath = function(path) {
        transitionPath = path;
    };
    var getTransitionPath = function() {
        var result = transitionPath;
        transitionPath = null;
        return result;
    };
    return ({
        id: id,
        user: user,
        cred: cred,
        isLoggedIn: isLoggedIn,
        addChangeListener: addChangeListener,
        removeChangeListener: removeChangeListener,
        setTransitionPath: setTransitionPath,
        getTransitionPath: getTransitionPath
    });
})();
module.exports = LoginStore;