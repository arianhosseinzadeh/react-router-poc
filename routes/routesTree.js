var React = require('react');
var Router = require('react-router');
var Route = Router.Route;
var DefaultRoute = Router.DefaultRoute;
var App = require('../components/App'),
    About = require('../components/About'),
    Products = require('../components/Products'),
    Login = require('../components/Login'),
    Home = require('../components/Home');

var routes = (
    <Route name="app" path="/" handler={App}>
        <Route path="about" name="about" handler={About} />
        <Route path="products" name="products" handler={Products} />
        <Route path="login" name="login" handler={Login} />
        <DefaultRoute name="home" handler={Home} />
    </Route>
);

module.exports = routes;