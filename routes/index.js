var React = require('react');
var Router = require('react-router');
var routes = require('./routesTree');
var Login = require('../components/Login');
var router = function(req, res, next) {
    var router = Router.create({
        routes: routes,
        onAbort: function(abortReason) {
            var url = router.makePath(abortReason.to, abortReason.params, abortReason.query);
            res.redirect(url);
        }
    });
    router.run(function(Handler, state) {
        var initialState = {};
        var markup = React.renderToString(<Handler {...initialState}/>);
        res.render('react-app', {
            markup: markup,
            state: JSON.stringify(initialState)
        });
    });
};
module.exports = router;
